/**
 * Created by Lesenyeho on 9/28/2015.
 */
import java.util.Scanner;
public class ReverseIt
{
    public static void main(String[] args)
    {
        String hold,reversed = " ";

        int[] array = {1,2,3,4,5};

        int swap;

        for(int x = 0; x < array.length/2; x++)
        {
            swap = array[x];

            array[x] = array[array.length - x - 1];//swapping the element of array

            array[array.length - x -1] = swap;
        }
        System.out.print("Reversed array is :");
        for(int i = 0;i < array.length;i++)
        {
            System.out.print(" " + array[i]);
        }
        System.out.println();
        Scanner in = new Scanner(System.in);

        System.out.print("Enter a String to be Reversed:");

        hold = in.nextLine();

        int length = hold.length() - 1;

        while (length >= 0)
        {
            reversed += hold.charAt(length);//assigning last character to be the first to start
            length--;//decrementing length
        }
        System.out.print("Reversed String is :" + reversed);

    }
}

