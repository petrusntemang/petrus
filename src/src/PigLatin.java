/**
 * Created by Lesenyeho on 10/6/2015.
 */
import java.util.Scanner;
public class PigLatin
{
    public static void main(String[] args)
    {
        System.out.println("Enter word to be translated: ");
        Scanner scanner = new Scanner(System.in);

        String findFirstVowel = scanner.nextLine();
        char v = Character.toLowerCase(findFirstVowel.charAt(0));

        if (v == 'a' || v == 'e' || v == 'i' || v == 'o' || v == 'u')
        {
            String convertToPigLatin = findFirstVowel.substring(0,findFirstVowel.length())+"-"+findFirstVowel.substring(1, 1);
            System.out.println(convertToPigLatin+"way");
        }
        else
        {
                String first = findFirstVowel.substring(1, findFirstVowel.length()) + "-" + findFirstVowel.substring(0, 1) + "ay";
                System.out.println(first);
        }

    }
}
