/**
 * Created by Lesenyeho on 10/5/2015.
 */
import java.util.Scanner;
public class  RBG

{
    public static void main(String[] args)
    {
       // R = 128, G = 192, and B = 79, then output #80C04F. R = 128, G = 192, and B = 79, then output #80C04F.
        //convert RGB to hexadecimal
        System.out.print("Enter number of red colour to convert to hexadecimal:");
        Scanner in = new Scanner(System.in);
        int r = in.nextInt();
        System.out.print("Enter number of green colour to convert to hexadecimal:");
        int g = in.nextInt();
        System.out.print("Enter number of blue colour to convert to hexadecimal:");
        int b = in.nextInt();

        System.out.print("The hexadeximal is"+":" +"#"+toBrowserHexValue(r)+toBrowserHexValue(g)+toBrowserHexValue(b));

    }
    private static String toBrowserHexValue(int number) {
        StringBuilder builder = new StringBuilder(Integer.toHexString(number & 0xff));
        while (builder.length() < 2) {
            builder.append("0");
        }
        return builder.toString().toUpperCase();
    }
}
