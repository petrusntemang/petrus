/**
 * Created by Lesenyeho on 9/22/2015.
 */
public class TwelveTimeTable
{
    public static void main(String[] args) {
        int multiple;

        for (int number = 1; number <= 12; number++){

            for (int count = 1; count <= 12; count++) {
                multiple = number * count;//multipliying and assigning to multiply variable

                System.out.println(count + "\tX\t" + number + "\t=\t" + multiple);//output
            }
            System.out.println(" ");
    }

    }


}
