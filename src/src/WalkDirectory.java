/**
 * Created by Lesenyeho on 10/11/2015.
 */
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;

public class WalkDirectory
{
    public static void main(String[] args)throws IOException{
        File file = new File("C:\\Users\\Lesenyeho\\Desktop\\Fundamental\\petrus\\src\\");

        File[] fileList = file.listFiles(new FileFilter() {

            public boolean accept(File pathname) {
                return pathname.getName().endsWith(".txt");
            }
        });

        if (fileList != null) {

            for (File f : fileList) {
                System.out.println("File in directory: " + f.getName());
            }
        }
    }
}
