import java.util.Scanner;

/**
 * Created by Lesenyeho on 10/11/2015.
 */
public class SieveEratosthenes
{
    public static void main(String[] args) {


        Scanner scan = new Scanner(System.in);

        System.out.println("Enter maximum number  : ");

        int number = scan.nextInt();

        int[] numbers = new int[number + 1];

        int count = 2;

        for (int Index = 2; Index <= number; Index++)
        {
            numbers[Index] = count;
            count++;
        }

        for(int numIndex = 2; (numIndex * numIndex) <= number; numIndex++ )
        {
            for (int x = (numIndex * numIndex); x <= number; x = x + numIndex) {
                numbers[x] = 0;
            }
        }
        System.out.println("Prime Numbers are:");
        for (int numIndex = 2; numIndex <= number; numIndex++) {
            if (numbers[numIndex] != 0) {
                System.out.print(" " + numbers[numIndex]);
            }
        }
    }
}
