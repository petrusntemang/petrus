/**
 * Created by Lesenyeho on 9/28/2015.
 */
import java.util.Scanner;

public class AlignColumns {
    public static void main(String[] args) {

        String[] lines = {"Given$a$text$file$of$many,$lines$where$fields$within$a$line",
                "are$delineated$by$a$single$dollar$character,$write$a$program",
                "that$aligns$each$column$of$fields$by$ensuring$that$words$in$each",
                "column$are$separated$by$at$least$one$space.",
                "further,$allow$for$each$word$in$a$column$to$be$either$left",
                "justified,$right$justified$or$center$justified$within$its$column."};

        Scanner in = new Scanner(System.in);

        System.out.print("Enter 1 for Right align,2 for Left align and 3 for Center align :");

        int x = in.nextInt();


        if(x==1) {
            RightAlign(lines);
        }
        else if(x==2) {
            LeftAlign(lines);
        }else if(x==3) {
            CenterAlign(lines);
        }else
        {
            System.out.print("Please follow intructions");
        }
    }

    public static void RightAlign(String[] paragram)
    {
        System.out.print("----------------------right align--------------------------");
        String[] info = null;

        for (int x = 0; x < paragram.length; x++) {
            info = paragram[x].split("\\$");
            for (String in : info) {
                System.out.printf("%15s", in);

            }
            System.out.println();

        }
    }

    public static void LeftAlign(String[] paragram)
    {
        System.out.println("----------------------Left align--------------------------");

        String[] info = null;

        for (int x = 0; x < paragram.length; x++) {
            info = paragram[x].split("\\$");
            for (String in : info) {
                System.out.printf("%-15s", in);
            }
         System.out.println();
        }
    }

    public static void CenterAlign(String[] paragram)
    {
        System.out.println("----------------------center align--------------------------");

        String[] info = null;
        int width = 20;
        for (String in : paragram) {
            info = in.split("\\$");

            for (String in2 : info)
            {

                int sizeTable = width - in2.length();
                int colstart = in2.length() + sizeTable / 2;

                in2 = String.format("%" + colstart + "s", in2);
                in2 = String.format("%-" + width + "s", in2);

                System.out.print(in2);
            }

            System.out.println();
        }
    }
}














