import java.util.ArrayList;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by Lesenyeho on 10/6/2015.
 */
import java.util.Random;
public class BullsAndCows {
    public static void main(String[] args) {

        int num = 0;

        String s;
        Random r = new Random();

        int limit = 0;
        while(hasDupes(limit =(r.nextInt(9000)+1000)));
        Scanner in = new Scanner(System.in);
        String limited= limit+"";
        int guesses = 0;
        boolean guess = false;

        do {
            int bulls = 0;
            int cows = 0;
            System.out.print("Guess 4 numbers without duplicates:");
            num = in.nextInt();
            try
            {
            if(hasDupes(num) || num < 1000) continue;
           }catch(InputMismatchException e){
            continue;
        }
            s = num+"";
            System.out.println("Random numbers:"+limited);
            printGuessedNumbers(s);

            for (int i = 0; i < 4; i++) {
                if (s.charAt(i) == limited.charAt(i)) {
                    bulls++;
                } else if (limited.contains(s.charAt(i) + "")) {
                    cows++;
                }

            }
            if (bulls == 4) {
                guess = true;
            } else {
                        System.out.println("bulls" + ">>" + bulls + " " + "cows" + ">> " + cows);
                    }





        } while (!guess);
        System.out.println("You won after " + guesses + " guesses!");
    }

    public static void printGuessedNumbers(String guessedNum) {
        System.out.println("Guessed numbers");

        for (int e = 0; e < guessedNum.length(); e++) {
            System.out.print(" " + guessedNum.charAt(e));
        }
        System.out.println();
    }

    public static boolean hasDupes(int num) {
        boolean[] digs = new boolean[10];
        while (num > 0) {
            if (digs[num % 10]) return true;
            digs[num % 10] = true;
            num /= 10;
        }
        return false;
    }
}

