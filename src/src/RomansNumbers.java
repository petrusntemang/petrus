/**
 * Created by Lesenyeho on 10/11/2015.
 */
import java.util.Scanner;
public class RomansNumbers
{
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);

        System.out.print("Enter the roman numerals either in additive form or subtractive form :");
        String input = in.nextLine();

        String[] romanV = input.split(",");

        int sum = 0;
        int finalV = 0;

        for(int x = 0;x < romanV.length;x++)
        {
            finalV = Integer.parseInt(romanV[x]);

            sum += finalV;
        }
       // System.out.println(decode(finalV));
    }
    public static int decode(String roman) {
        int result = 0;
        String uRoman = roman.toUpperCase();
        for(int i = 0;i < uRoman.length() - 1;i++) {//loop over all but the last character
            //if this character has a lower value than the next character
            if (decodingRoman(uRoman.charAt(i)) < decodingRoman(uRoman.charAt(i + 1))) {
                //subtractive form
                result -= decodingRoman(uRoman.charAt(i));
            } else {
                //additive form
                result += decodingRoman(uRoman.charAt(i));
            }
        }
        //decode the last character, which is always added
        result += decodingRoman(uRoman.charAt(uRoman.length() - 1));
        return result;
    }
    private static int decodingRoman(char inputChar) {
        switch(inputChar) {
            case 'M': return 1000;
            case 'D': return 500;
            case 'C': return 100;
            case 'L': return 50;
            case 'X': return 10;
            case 'V': return 5;
            case 'I': return 1;
            default: return 0;
        }
    }
}
