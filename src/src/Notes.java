import java.io.*;
import java.io.IOException;
import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Lesenyeho on 10/8/2015.
 */
public class Notes
{
    public static void main(String[] args)
    {
        try
        {
            File file = new File("NOTES.TXT");

            if(!file.exists()) {
                file.createNewFile();
            }

                FileWriter fr = new FileWriter("NOTES.TXT");
                BufferedWriter Br = new BufferedWriter(fr);
                Br.write("Argument in this");
                Br.newLine();
                DateFormat dateFormat = new SimpleDateFormat("YYYY/MM/DD HH:mm:ss");
                Date date = new Date();
                Br.append(dateFormat.format(date));
                Br.newLine();
                Br.close();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }




    }
}
