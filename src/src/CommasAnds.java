package complete;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Lesenyeho on 10/8/2015.
 */
public class CommasAnds
{
    public static void main(String[] args) throws Exception {
        final List<String> words = Arrays.asList(new String[]{"ABC", "DEF", "G", "H"});

        final Iterator<String> wordIterator = words.iterator();
        final StringBuilder out = new StringBuilder();
        while (wordIterator.hasNext()) {
            out.append(wordIterator.next());
            if (wordIterator.hasNext()) {
                out.append(",");
            }
        }
        System.out.println(out.toString());
    }
}
