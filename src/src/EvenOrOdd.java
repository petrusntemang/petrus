/**
 * Created by Lesenyeho on 9/22/2015.
 */
import java.util.Scanner;
public class EvenOrOdd
{
    public static void main(String[] args)
    {
        int x;

        Scanner input = new Scanner(System.in);

        System.out.print("to check number is a even Or Odd using Remander press 1 or 2 for bitwise operator :");

        x=input.nextInt();
        if(x==1)
        {
            evenOrOdd();
        }
        else if(x == 2)
        {
            evenOdd();
        }
        else
        {
            System.out.print("wrong input");
        }
    }
    public static void evenOrOdd()
    {

        Scanner input = new Scanner(System.in);

        System.out.print("Enter a number and check weather its Even or Odd:");

        int num = input.nextInt();

        int Remainder = num % 2;

        if (Remainder == 0) {
            System.out.print("This number is Even");
        } else {
            System.out.print("This number is Odd");
        }

    }
    public static void evenOdd()
    {
        Scanner input = new Scanner(System.in);

        System.out.print("Enter a number and check weather its Even or Odd using bitwise operator:");

        int num = input.nextInt();

        if((num & 1)== 1)
        {
            System.out.print("This number is Odd");
        }
        else
        {
            System.out.print("This number is Even");
        }
    }
}
