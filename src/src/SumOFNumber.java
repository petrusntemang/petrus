/**
 * Created by Lesenyeho on 9/22/2015.
 */
public class SumOFNumber
{
    public static void main(String[] args)
    {
        int[] numbers = {2,4,6,8,9,6,8,10};//declaring array and assigning values to it

        int sum = 0;//declaring sum variable

        for(int count = 0; count < numbers.length; count++)
        {
            sum += numbers[count];//adding values of array and assigning them to sum variable
        }
        System.out.println("The sum is :"+ sum);//output
    }
}
