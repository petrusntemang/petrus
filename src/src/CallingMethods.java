import java.util.ArrayList;

/**
 * Created by Lesenyeho on 10/2/2015.
 */
public class CallingMethods
{
    public static void main(String[] args){

        Method();

        int f = 3;

        FixedNumber(f);

        optionalArguments(6,"");

        int r = ReturnMethod();

        privateMethod();

        protectedMethod();

        variableNumber(3);


    }
    public static void Method(){}

    public static void FixedNumber(int f){

        System.out.print(f);
    }

    public static void optionalArguments(int x,String p){}

    public static void variableNumber(int x){}

    public static int ReturnMethod()
    {
        int p = 0;

        return  p;
    }
    public static void RefPass (ArrayList<String> List){}

    private static void privateMethod(){}

    protected static void protectedMethod(){}


}

