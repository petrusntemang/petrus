/**
 * Created by Lesenyeho on 9/23/2015.
 */
import java.io.*;
/*This program create new file and delete it*/
public class DeleteFile
{
    public static void main(String[] args)
    {
        try {
            File file = new File("C:\\Users\\Lesenyeho\\Desktop\\Fundamental\\petrus\\src\\src\\input.txt");//specifying location for the file
            file.createNewFile();//creating the file
            File file1 = new File("C:\\Users\\Lesenyeho\\Desktop\\Fundamental\\petrus\\src\\src\\docs");
            file1.mkdir();
            if(file.exists())
            {
                file.delete();//deleting the file
            }
            if(file1.exists())
            {
               file1.delete();//deleting docs
            }
        }catch(IOException e)
        {
           e.printStackTrace();//catching exception
        }
    }
}
